<?php return array (
  'root' => 
  array (
    'pretty_version' => 'dev-main',
    'version' => 'dev-main',
    'aliases' => 
    array (
    ),
    'reference' => '5a15450653bcb253af37f53d00b36fff0280300c',
    'name' => 'pattern-lab/edition-drupal-standard',
  ),
  'versions' => 
  array (
    'alchemy/zippy' => 
    array (
      'pretty_version' => '0.3.5',
      'version' => '0.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '92c773f7bbe47fdb30c61dbaea3dcbf4dd13a40a',
    ),
    'aleksip/plugin-data-transform' => 
    array (
      'pretty_version' => 'v1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cb372612ed886608348e246a5c523e0a6fa45a22',
    ),
    'doctrine/collections' => 
    array (
      'pretty_version' => 'v1.4.0',
      'version' => '1.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '1a4fb7e902202c33cce8c55989b945612943c2ba',
    ),
    'drupal/core-render' => 
    array (
      'pretty_version' => '8.9.16',
      'version' => '8.9.16.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd20b0afe9ca3ca768c6e828442e4cdebbb2cfeec',
    ),
    'drupal/core-utility' => 
    array (
      'pretty_version' => '8.9.16',
      'version' => '8.9.16.0',
      'aliases' => 
      array (
      ),
      'reference' => '14cc3fae9c02333ae7f47503065a24c5e2794524',
    ),
    'kevinlebrun/colors.php' => 
    array (
      'pretty_version' => '1.0.3',
      'version' => '1.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cdda5eee41314b87cd5a8bb91b1ffc7c0210e673',
    ),
    'michelf/php-markdown' => 
    array (
      'pretty_version' => '1.9.0',
      'version' => '1.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c83178d49e372ca967d1a8c77ae4e051b3a3c75c',
    ),
    'pattern-lab/core' => 
    array (
      'pretty_version' => 'v2.9.0',
      'version' => '2.9.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '009399c5cb175f1d5a1fafc7b07abc1f2d13a0a3',
    ),
    'pattern-lab/drupal-twig-components' => 
    array (
      'pretty_version' => 'v2.2.0',
      'version' => '2.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3f4e2ad80051ac66950c97e6a07e08376b001614',
    ),
    'pattern-lab/edition-drupal-standard' => 
    array (
      'pretty_version' => 'dev-main',
      'version' => 'dev-main',
      'aliases' => 
      array (
      ),
      'reference' => '5a15450653bcb253af37f53d00b36fff0280300c',
    ),
    'pattern-lab/patternengine-twig' => 
    array (
      'pretty_version' => 'v2.2.2',
      'version' => '2.2.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'edeb27a56a7a389e8f369e02ba9c50ee83a84cbb',
    ),
    'pattern-lab/styleguidekit-assets-default' => 
    array (
      'pretty_version' => 'v3.5.2',
      'version' => '3.5.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '7c1b51195c17a19cc3e87890b8fb3d8c27f33870',
    ),
    'pattern-lab/styleguidekit-twig-default' => 
    array (
      'pretty_version' => 'v3.0.0',
      'version' => '3.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7b38bfe2fb60cde3850cfe8f4f948cdc206d7188',
    ),
    'seld/jsonlint' => 
    array (
      'pretty_version' => '1.8.3',
      'version' => '1.8.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '9ad6ce79c342fbd44df10ea95511a1b24dee5b57',
    ),
    'shudrum/array-finder' => 
    array (
      'pretty_version' => 'v1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '42380f01017371b7a1e8e02b0bf12cb534e454d7',
    ),
    'symfony/event-dispatcher' => 
    array (
      'pretty_version' => 'v3.4.47',
      'version' => '3.4.47.0',
      'aliases' => 
      array (
      ),
      'reference' => '31fde73757b6bad247c54597beef974919ec6860',
    ),
    'symfony/filesystem' => 
    array (
      'pretty_version' => 'v3.4.47',
      'version' => '3.4.47.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e58d7841cddfed6e846829040dca2cca0ebbbbb3',
    ),
    'symfony/finder' => 
    array (
      'pretty_version' => 'v3.4.47',
      'version' => '3.4.47.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b6b6ad3db3edb1b4b1c1896b1975fb684994de6e',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '46cd95797e9df938fdd2b03693b5fca5e64b01ce',
    ),
    'symfony/process' => 
    array (
      'pretty_version' => 'v3.4.47',
      'version' => '3.4.47.0',
      'aliases' => 
      array (
      ),
      'reference' => 'b8648cf1d5af12a44a51d07ef9bf980921f15fca',
    ),
    'symfony/yaml' => 
    array (
      'pretty_version' => 'v3.4.47',
      'version' => '3.4.47.0',
      'aliases' => 
      array (
      ),
      'reference' => '88289caa3c166321883f67fe5130188ebbb47094',
    ),
    'twig/twig' => 
    array (
      'pretty_version' => 'v1.44.4',
      'version' => '1.44.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '4d400421528e9fa40caaffcf7824c172526dd99d',
    ),
  ),
);
